from subprocess import call
import os
import datetime

def RunPythonTest(scriptName, testName, otherParameters):
    inputFilename = testName + "Input.txt"
    outputFilename = "out.txt"
    targetFilename = testName + "Output.txt"

    callVector = ["python.exe", scriptName] + otherParameters + [inputFilename, outputFilename]
    
    startTime = datetime.datetime.now()
    call(callVector)
    endTime = datetime.datetime.now()
    duration = endTime - startTime
    result = CompareFiles(targetFilename, outputFilename)    
    if (result):
        print "%s: %s: Test Passed. Running time: %f seconds" % (testName, otherParameters[0], duration.total_seconds())
    else:
        print "%s: %s: Test Failed. Running time: %f seconds" % (testName, otherParameters[0], duration.total_seconds())
    
    os.remove(outputFilename)
    
    
def CompareFiles(targetFilename, resultFilename):
    resultFile = open(resultFilename, 'r')
    targetFile = open(targetFilename, 'r')
    resultLines = resultFile.readlines()
    targetLines = targetFile.readlines()
    resultFile.close()
    targetFile.close()
    
    result = True
    for i in range(max(len(resultLines), len(targetLines))):    
        if i >= len(resultLines):
            print "Line %d: |%s| <> None" % (i, targetLines[i].strip("\n"))
            result = False
        elif i >= len(targetLines):
            print "Line %d: None <> |%s|" %(i, resultLines[i].strip("\n"))
            result = False        
        elif (targetLines[i] != resultLines[i]):
            print "Line %d: |%s| <> |%s|" % (i, targetLines[i].strip("\n"), resultLines[i].strip("\n"))
            result = False
            
    return result
    
if __name__ == "__main__":
    script = "SpanningTreeGenerator.py"   
    algorithms = ["quickfind"]
    for algorithm in algorithms:
        RunPythonTest(script, "simpleTest", [algorithm, "10"])
        RunPythonTest(script, "smallTest1000", [algorithm, "1000"])
        RunPythonTest(script, "smallTest2500", [algorithm, "2500"])
        RunPythonTest(script, "mediumTest10000", [algorithm, "10000"])
        if (algorithm != "quickfind") and (algorithm != "quickunion"):
            RunPythonTest(script, "largeTest100000", [algorithm, "10000"])
    
    
