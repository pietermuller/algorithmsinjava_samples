import sys
import random

def Generate(numberOfNodes, numberOfPairs, outputFilename):
    outFile = open(outputFilename, 'w')
    
    for i in range(numberOfPairs):
        val1 = random.randint(0, numberOfNodes-1)
        val2 = random.randint(0, numberOfNodes-1)
    
        print >> outFile, val1, val2
        
    outFile.close()



if __name__ == "__main__":
    if len(sys.argv) == 4:
        numberOfNodes = int(sys.argv[1])
        numberOfPairs = int(sys.argv[2])
        outputFilename = sys.argv[3]
        
        Generate(numberOfNodes, numberOfPairs, outputFilename)
    else:
        print "Check parameters"
        