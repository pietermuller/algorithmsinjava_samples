import sys
import os

# ALGORITHMS:    
    
def GenerateSpanningTree_QuickFind(numberOfNodes, inputTuples):    
    global log
    
    nodes = range(numberOfNodes)
    
    for (p,q) in inputTuples:
        # Find:
        t = nodes[p]
        if (nodes[q] == t):
            # Already matched
            continue
        
        # Output:
        log.Print("%d %d" %(p, q))
        
        # Union:
        # change anything pointing to p to point to q:
        for i in range(numberOfNodes):
            if nodes[i] == t:
                nodes[i] = nodes[q]

# HELPERS:                

log = None

class Log:    
    def __init__(self, filename = None):
        if filename != None:
            self.file = open(filename, 'w')
        
    def Print(self, msg):
        print >> self.file, msg
        
    def Close(self):
        self.file.close()

def InitOutput(outputFilename):
    global log
    log = Log(outputFilename)
    
def CloseOutput():
    global log
    log.Close()

def GetInput(filename):
    """ Returns a list of tuples """   
    global log    
    
    
    if (not os.path.exists(filename)):
        raise Exception("Input file does not exist.")
    
    infile = open(filename, 'r')
    
    toret = []
    
    for line in infile.readlines():
        tokens = line.split()
        
        if len(tokens) != 2:
            raise Exception("Expected two tokens per line")
        
        try:
            val1 = int(tokens[0])
            val2 = int(tokens[1])
        except:
            raise Exception("Could not parse input. Integers expected.")
        
        tup = (val1, val2)
        
        toret.append(tup)
        
    return toret
    


                
def GetAlgorithm(algorithm):
    if (algorithm == "quickfind"):
        return GenerateSpanningTree_QuickFind
    else:
        raise Exception("Unknown algorithm")
                
if __name__ == "__main__":
    try:    
        if len(sys.argv) == 5:
            algorithm = sys.argv[1]
            generatorMethod = GetAlgorithm(algorithm)
            numberOfNodes = int(sys.argv[2])
            inputFilename = sys.argv[3]
            outputFilename = sys.argv[4]    
            
            InitOutput(outputFilename)
            tuples = GetInput(inputFilename)            
            generatorMethod(numberOfNodes, tuples)            
            CloseOutput()
            
        else:
            print "Parameters Required:"
            print "\tAlgorithm name"
            print "\tNumber of Nodes"
            print "\tInput Filename"
            print "\tOutput Filename"
            raise Exception("Parameters error")
    except Exception, e:
        print e
        print "FAIL"